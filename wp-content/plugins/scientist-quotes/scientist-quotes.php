<?php
/*
Plugin Name: Citations de Scientifiques
Description: Affiche des citations de grands scientifiques et chercheurs dans l'interface d'administration.
Version: 1.0
*/

function citations_scientifiques()
{
    $citations = array(
        "Albert Einstein" => "L'imagination est plus importante que le savoir.",
        "Marie Curie" => "Rien dans la vie ne doit être craint, tout doit être compris.",
        "Stephen Hawking" => "Le plus grand ennemi de la connaissance n'est pas l'ignorance, c'est l'illusion de la connaissance.",
        "Carl Sagan" => "Quelque part, quelque chose d'incroyable attend d'être découvert.",
        "Isaac Newton" => "Si j'ai vu plus loin, c'est en me tenant sur les épaules de géants.",
        "Jane Goodall" => "Le moins que je puisse faire, c'est de parler pour ceux qui ne peuvent pas parler pour eux-mêmes.",
        "Richard Feynman" => "Le premier principe est que vous ne devez pas vous tromper, et vous êtes la personne la plus facile à tromper.",
        "Ada Lovelace" => "Je ne suis pas du tout dégoûtée par mon association avec la machine, ni par les capacités que je possède pour abstraire des sentiments mentaux à partir d'une impression physique.",
        "Nikola Tesla" => "Le présent leur appartient ; le futur, pour lequel j'ai vraiment travaillé, m'appartient.",
        "Rosalind Franklin" => "La science et la vie quotidienne ne peuvent ni ne doivent être séparées.",
    );

    $scientifique = array_rand($citations);
    $citation = $citations[$scientifique];

    echo '<div class="citation-scientifique">';
    echo '<p><strong>' . esc_html($scientifique) . ' :</strong> ' . esc_html($citation) . '</p>';
    echo '</div>';
}
add_action('admin_notices', 'citations_scientifiques');
