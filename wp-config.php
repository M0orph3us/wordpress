<?php

/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('WP_CACHE', true);
define('WPCACHEHOME', 'C:\wamp64\www\wordpress\wp-content\plugins\wp-super-cache/');
define('DB_NAME', 'refonte');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'wordpress');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'wordpress');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&+.IloA7mx ~MjR^g/(14q*?9l!yy`f2f>3#W8dNw5iN$ 1Qd:=sQ^et/wlOz$gd');
define('SECURE_AUTH_KEY',  'Ecmd_fxO3>@Z[#NmZP<jN]j%}DzE&QXq!p64,BXGvoy .3Fb%,54LSg206:5R@n1');
define('LOGGED_IN_KEY',    'I%,)F,%3ZnUSZ{3vX1/uexX,r]6}|}_{`W% 6X#q!Sad4]M;4F-uktEjAwU,)$vH');
define('NONCE_KEY',        '4$v1-1v[S(qsXq)4N&24WZM|NjMu7Au_pR#5q`L,I*~%SL>}e}|+&??%cO5^c3ac');
define('AUTH_SALT',        'Sul!1o#M=ywW5+h:U6w:dz;[T|1lsYw<zQ8AB`PE% h0WCzW3:z87{??K@R4~Dlx');
define('SECURE_AUTH_SALT', 'z0U@*+EB;6UHii@y8,[#?!A%RjmP}_{$DL/(z+/r)-K3/qgSK~XIvL<>,%4:@Cby');
define('LOGGED_IN_SALT',   'N4`*Tqai}-#uOCI?O%-PC9LKRTYBwH`!6DV?8y(&Z-DI:ytz@G=(i`}e[In~pB([');
define('NONCE_SALT',       'Ynp7pKO7k2Pl:Kz[<;AT(M9J~qXx&)fW0}FOm}kt.;o:&L/F9l/bm~Dtic$}{E<&');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if (!defined('ABSPATH'))
  define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
